# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Chetan Khona <chetan@kompkin.com>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:26+0000\n"
"PO-Revision-Date: 2013-03-13 11:56+0530\n"
"Last-Translator: Chetan Khona <chetan@kompkin.com>\n"
"Language-Team: Marathi <kde-i18n-doc@kde.org>\n"
"Language: mr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Lokalize 1.5\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "चेतन खोना"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "chetan@kompkin.com"

#: kminfo.cpp:55
#, kde-format
msgid "KDE version of Oyranos Synnefo"
msgstr ""

#: kminfo.cpp:57
#, kde-format
msgid "(c) 2008 Joseph Simon III"
msgstr ""

#: kminfo.cpp:61
#, kde-format
msgid "Joseph Simon III"
msgstr ""

#: kminfo.cpp:61 kminfo.cpp:62 kminfo.cpp:63 kminfo.cpp:65 kminfo.cpp:66
#: kminfo.cpp:67 kminfo.cpp:68 kminfo.cpp:69 kminfo.cpp:70 kminfo.cpp:71
#, kde-format
msgid "developer"
msgstr ""

#: kminfo.cpp:62
#, kde-format
msgid "Kai-Uwe Behrmann"
msgstr ""

#: kminfo.cpp:63
#, kde-format
msgid "Jan Gruhlich"
msgstr ""

#: kminfo.cpp:65
#, kde-format
msgid "Albert Astals Cid"
msgstr ""

#: kminfo.cpp:66
#, kde-format
msgid "Christoph Feck"
msgstr ""

#: kminfo.cpp:67
#, kde-format
msgid "Boudewijn Rempt "
msgstr ""

#: kminfo.cpp:68
#, kde-format
msgid "Pino Toscano"
msgstr ""

#: kminfo.cpp:69
#, kde-format
msgid "Laurent Montel"
msgstr ""

#: kminfo.cpp:70
#, kde-format
msgid "Cyrille Berger Skott"
msgstr ""

#: kminfo.cpp:71
#, kde-format
msgid "Hal Van Engel"
msgstr ""

#~ msgid "in memory"
#~ msgstr "स्मृती अंतर्गत"

#~ msgid "Installed Profiles"
#~ msgstr "प्रतिष्ठापीत केलेल्या रूपरेषा"

#~ msgid "Description"
#~ msgstr "वर्णन"

#~ msgid "Devices"
#~ msgstr "साधने"

#~ msgid "Editing Space"
#~ msgstr "संपादनाची जागा"

#~ msgid "----"
#~ msgstr "----"

#~ msgid "Device Class:"
#~ msgstr "साधन वर्ग :"

#~ msgid "Manufacturer:"
#~ msgstr "उत्पादक :"

#~ msgid "Model:"
#~ msgstr "मॉडेल :"

#~ msgid "Color Space:"
#~ msgstr "रंग जागा :"

#~ msgid "Profile Path:"
#~ msgstr "रूपरेषा मार्ग :"

#~ msgid "---"
#~ msgstr "---"
