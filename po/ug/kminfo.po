# Uyghur translation for kminfo.
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Gheyret Kenji <gheyret@gmail.com>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: kminfo\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:26+0000\n"
"PO-Revision-Date: 2013-09-08 07:04+0900\n"
"Last-Translator: Gheyret Kenji <gheyret@gmail.com>\n"
"Language-Team: Uyghur <kde-i18n-doc@kde.org>\n"
"Language: ug\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "ئابدۇقادىر ئابلىز, غەيرەت كەنجى"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "sahran.ug@gmail.com,  gheyret@gmail.com"

#: kminfo.cpp:55
#, kde-format
msgid "KDE version of Oyranos Synnefo"
msgstr ""

#: kminfo.cpp:57
#, kde-format
msgid "(c) 2008 Joseph Simon III"
msgstr "(c) 2008 Joseph Simon III"

#: kminfo.cpp:61
#, fuzzy, kde-format
#| msgid "(c) 2008 Joseph Simon III"
msgid "Joseph Simon III"
msgstr "(c) 2008 Joseph Simon III"

#: kminfo.cpp:61 kminfo.cpp:62 kminfo.cpp:63 kminfo.cpp:65 kminfo.cpp:66
#: kminfo.cpp:67 kminfo.cpp:68 kminfo.cpp:69 kminfo.cpp:70 kminfo.cpp:71
#, kde-format
msgid "developer"
msgstr ""

#: kminfo.cpp:62
#, fuzzy, kde-format
#| msgid "2010-2013 Kai-Uwe Behrmann"
msgid "Kai-Uwe Behrmann"
msgstr "2010-2013 Kai-Uwe Behrmann"

#: kminfo.cpp:63
#, kde-format
msgid "Jan Gruhlich"
msgstr ""

#: kminfo.cpp:65
#, kde-format
msgid "Albert Astals Cid"
msgstr ""

#: kminfo.cpp:66
#, kde-format
msgid "Christoph Feck"
msgstr ""

#: kminfo.cpp:67
#, kde-format
msgid "Boudewijn Rempt "
msgstr ""

#: kminfo.cpp:68
#, kde-format
msgid "Pino Toscano"
msgstr ""

#: kminfo.cpp:69
#, kde-format
msgid "Laurent Montel"
msgstr ""

#: kminfo.cpp:70
#, kde-format
msgid "Cyrille Berger Skott"
msgstr ""

#: kminfo.cpp:71
#, kde-format
msgid "Hal Van Engel"
msgstr ""

#~ msgid "2008-2009 Joseph Simon III"
#~ msgstr "2008-2009 Joseph Simon III"

#~ msgid "Description"
#~ msgstr "چۈشەندۈرۈش"

#~ msgid "Devices"
#~ msgstr "ئۈسكۈنىلەر"

#~ msgid "----"
#~ msgstr "----"

#~ msgid "Manufacturer:"
#~ msgstr "ياسىغۇچى:"

#~ msgid "Model:"
#~ msgstr "تىپى:"

#~ msgid "---"
#~ msgstr "---"
