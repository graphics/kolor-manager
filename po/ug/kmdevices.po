# Uyghur translation for kmdevices.
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Gheyret Kenji <gheyret@gmail.com>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: kmdevices\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:26+0000\n"
"PO-Revision-Date: 2013-09-08 07:04+0900\n"
"Last-Translator: Gheyret Kenji <gheyret@gmail.com>\n"
"Language-Team: Uyghur <kde-i18n-doc@kde.org>\n"
"Language: ug\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "ئابدۇقادىر ئابلىز, غەيرەت كەنجى"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "sahran.ug@gmail.com,  gheyret@gmail.com"

#: kmdevices.cpp:51
#, kde-format
msgid "KDE version of Oyranos Synnefo"
msgstr ""

#: kmdevices.cpp:53
#, kde-format
msgid "(c) 2008 Joseph Simon III"
msgstr "(c) 2008 Joseph Simon III"

#: kmdevices.cpp:57
#, fuzzy, kde-format
#| msgid "(c) 2008 Joseph Simon III"
msgid "Joseph Simon III"
msgstr "(c) 2008 Joseph Simon III"

#: kmdevices.cpp:57 kmdevices.cpp:58 kmdevices.cpp:59 kmdevices.cpp:61
#: kmdevices.cpp:62 kmdevices.cpp:63 kmdevices.cpp:64 kmdevices.cpp:65
#: kmdevices.cpp:66 kmdevices.cpp:67
#, kde-format
msgid "developer"
msgstr ""

#: kmdevices.cpp:58
#, fuzzy, kde-format
#| msgid "2010-2013 Kai-Uwe Behrmann"
msgid "Kai-Uwe Behrmann"
msgstr "2010-2013 Kai-Uwe Behrmann"

#: kmdevices.cpp:59
#, kde-format
msgid "Jan Gruhlich"
msgstr ""

#: kmdevices.cpp:61
#, kde-format
msgid "Albert Astals Cid"
msgstr ""

#: kmdevices.cpp:62
#, kde-format
msgid "Christoph Feck"
msgstr ""

#: kmdevices.cpp:63
#, kde-format
msgid "Boudewijn Rempt "
msgstr ""

#: kmdevices.cpp:64
#, kde-format
msgid "Pino Toscano"
msgstr ""

#: kmdevices.cpp:65
#, kde-format
msgid "Laurent Montel"
msgstr ""

#: kmdevices.cpp:66
#, kde-format
msgid "Cyrille Berger Skott"
msgstr ""

#: kmdevices.cpp:67
#, kde-format
msgid "Hal Van Engel"
msgstr ""

#~ msgid "2008-2009 Joseph Simon III"
#~ msgstr "2008-2009 Joseph Simon III"

#~ msgid "Internal error"
#~ msgstr "ئىچكى خاتالىق"

#~ msgid "automatic"
#~ msgstr "ئاپتوماتىك"

#~ msgid "Device Name"
#~ msgstr "ئۈسكۈنە ئاتى"
